package com.intutrack.myntradockmanager

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavHost
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.intutrack.intutracksct.Retrofit.Helper.APIUtility
import com.intutrack.myntradockmanager.Bean.BaseResponse
import com.intutrack.myntradockmanager.Bean.Docks.DockOutRequest
import com.intutrack.myntradockmanager.Bean.Docks.DockResponse
import com.intutrack.myntradockmanager.Bean.Docks.DocksResult
import com.intutrack.myntradockmanager.Bean.OpenTransactions.ContainerId
import com.intutrack.myntradockmanager.Bean.OpenTransactions.OpenTransactionResult
import com.intutrack.myntradockmanager.Bean.OpenTransactions.OpenTransactionsResponse
import com.intutrack.myntradockmanager.Bean.UpdateTrip.UpdateLoadingEnd
import com.intutrack.myntradockmanager.Bean.UpdateTrip.UpdateLoadingStart
import com.intutrack.myntradockmanager.UI.Adapters.DockAdapter
import com.intutrack.myntradockmanager.UI.Adapters.DockOutAdapter
import com.intutrack.myntradockmanager.UI.Adapters.LoadingAdapter
import com.intutrack.myntradockmanager.UI.Adapters.OpenTransactionAdapter
import com.intutrack.myntradockmanager.UI.AssignDockActivity
import com.intutrack.myntradockmanager.UI.AssignVehicleActivity
import com.intutrack.myntradockmanager.UI.BaseActivity
import com.intutrack.myntradockmanager.UI.TripActivity
import com.intutrack.myntradockmanager.Utilities.CommonUtils
import com.intutrack.myntradockmanager.databinding.ActivityMainBinding

class MainActivity : BaseActivity()/*, DockAdapter.DockAdapterCallbacks,
    OpenTransactionAdapter.OpenTransactionAdapterCallback,
    TextWatcher, CompoundButton.OnCheckedChangeListener,
    LoadingAdapter.LoadingAdapterCallbacks*/ {
    private val activityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    /*  private var tabLayout: TabLayout? = null
      private var tabSelection = 0
      lateinit var rv_home: RecyclerView
      lateinit var fab: FloatingActionButton
      lateinit var fab_logout: FloatingActionButton
      lateinit var et_search: EditText


      private val ar_incoming: ArrayList<OpenTransactionResult> = ArrayList()
      private val ar_outgoing: ArrayList<OpenTransactionResult> = ArrayList()
      private var sw_incoming: SwitchCompat? = null
      private var ll_sw: LinearLayout? = null*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activityMainBinding.root)
        val navhostfragment = supportFragmentManager.findFragmentById(R.id.main_Fragment_Container) as NavHostFragment
        val navController = navhostfragment.navController
        activityMainBinding.bottomNavMenu.setupWithNavController(navController)
        activityMainBinding.mainToolbar.setupWithNavController(navController)

        /*    init()*/
    }

    /*  fun init() {
  *//*        tabLayout = findViewById(R.id.tabs)
        et_search = findViewById(R.id.et_search_home)
        rv_home = findViewById(R.id.rv_home)*//*
        rv_home.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


//        sw_incoming = findViewById(R.id.sw_incoming)
        sw_incoming!!.setOnCheckedChangeListener(this)
    *//*    ll_sw = findViewById(R.id.ll_sw)

        fab = findViewById(R.id.fab)
        fab_logout = findViewById(R.id.fab_logout)*//*
        fab_logout.setOnClickListener {
            logout()
        }
        fab.setOnClickListener {
            showNewTripDialog()
        }

        et_search.addTextChangedListener(this)

        setupTabs()
    }*/

    override fun onResume() {
        super.onResume()
        /*     val tab = tabLayout!!.getTabAt(0)
             tab!!.select()
             GetOpenTransactions()*/
    }

    /*  fun setupTabs() {
          tabLayout!!.addTab(tabLayout!!.newTab().setText("Open Transactions"))
          tabLayout!!.addTab(tabLayout!!.newTab().setText("Docks"))
          tabLayout!!.addTab(tabLayout!!.newTab().setText("Loading/Unloading"))

          tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
              override fun onTabReselected(p0: TabLayout.Tab?) {
              }

              override fun onTabUnselected(p0: TabLayout.Tab?) {

              }

              override fun onTabSelected(tab: TabLayout.Tab) {

                  when (tab.position) {
                      0 -> {
                          ll_sw!!.visibility = View.VISIBLE
                          GetOpenTransactions()
                      }
                      1 -> {
                          ll_sw!!.visibility = View.GONE
                          GetDocks()
                      }
                      2 -> {
                          ll_sw!!.visibility = View.GONE
                          GetLoadingData()
                      }

                  }
              }
          })
      }


      fun GetOpenTransactions() {
          apiUtility.GetOpenTransactions(
              this,
              true,
              object : APIUtility.APIResponseListener<OpenTransactionsResponse> {
                  override fun onReceiveResponse(response: OpenTransactionsResponse?) {
                      ar_incoming.clear()
                      ar_outgoing.clear()
                      rv_home.adapter = OpenTransactionAdapter(this@MainActivity, response!!.result)
                      for (i in response.result) {
                          if (i.vehicle_status.equals(
                                  "incoming",
                                  true
                              )
                          ) ar_incoming.add(i) else ar_outgoing.add(i)
                      }
                      rv_home.adapter = OpenTransactionAdapter(
                          this@MainActivity,
                          if (sw_incoming!!.isChecked) ar_incoming else ar_outgoing
                      )
                  }
              })
      }

      fun GetDocks() {
          apiUtility.GetDocks(this, true, object : APIUtility.APIResponseListener<DockResponse> {
              override fun onReceiveResponse(response: DockResponse?) {
                  rv_home.adapter = DockAdapter(this@MainActivity, response!!.result)
              }
          })
      }

      fun GetLoadingData(){
          apiUtility.GetDocks(this, true, object : APIUtility.APIResponseListener<DockResponse> {
              override fun onReceiveResponse(response: DockResponse?) {
                  var loading: ArrayList<DocksResult> = ArrayList()
                  for(item in response!!.result){
                      if (item.isOccupied)
                          loading.add(item)
                  }
                  rv_home.adapter = LoadingAdapter(this@MainActivity, loading)
              }
          })
      }

      fun UpdateLoadingStart(data: DocksResult, updateLoadingStart: UpdateLoadingStart){
          apiUtility.UpdateLoadingStart(this,true,updateLoadingStart,data.ongoing_trip_data._id, object : APIUtility.APIResponseListener<BaseResponse>{
              override fun onReceiveResponse(response: BaseResponse?) {
                  GetLoadingData()
              }
          })
      }

      fun UpdateLoadingEnd(data: DocksResult, updateLoadingEnd: UpdateLoadingEnd){
          apiUtility.UpdateLoadingEnd(this,true,updateLoadingEnd,data.ongoing_trip_data._id, object : APIUtility.APIResponseListener<BaseResponse>{
              override fun onReceiveResponse(response: BaseResponse?) {
                  GetLoadingData()
              }

          })
      }

      override fun AssignDock(data: OpenTransactionResult) {
          var intent = Intent(this, AssignDockActivity::class.java)
          intent.putExtra("data", data)
          startActivity(intent)
      }

      override fun OnDockIn(data: DocksResult) {
          if (data.tripList.size > 0) {
              var intent = Intent(this, AssignVehicleActivity::class.java)
              intent.putExtra("data", data.tripList)
              intent.putExtra("dockId", data._id)
              startActivity(intent)
          } else {
              CommonUtils.alert(this, "No Available Transaction on this Dock")
          }

      }

      override fun OnDockOut(data: DocksResult) {
          CommonUtils.log("DATA", Gson().toJson(data))
      //showContainerDialog(data)
          DockOut(null, data.ongoing_trip_data._id, data._id, DockOutRequest())
      }

      fun DockOut(dialog: Dialog?, tripId: String, dockId: String, dockrequest: DockOutRequest) {
          apiUtility.DockOut(
              this,
              true,
              dockrequest,
              dockId,
              tripId,
              object : APIUtility.APIResponseListener<BaseResponse> {
                  override fun onReceiveResponse(response: BaseResponse?) {
                      CommonUtils.alert(this@MainActivity, response!!.message)
                      dialog?.dismiss()
                      GetDocks()
                  }
              })
      }


      override fun OnLoadingClick(data: DocksResult) {
          UpdateLoadingStart(data, UpdateLoadingStart(System.currentTimeMillis()))
      }

      override fun OnUnloadingClick(data: DocksResult) {
          showUnloadingDialog(data)
      }*/


/*
    fun showContainerDialog(data: DocksResult) {
        var dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_container)

        var cancel: Button = dialog.findViewById(R.id.btn_cancel)
        var dock_out: Button = dialog.findViewById(R.id.btn_dock_out)
        var rv_container: RecyclerView = dialog.findViewById(R.id.rv_containers)

        rv_container.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_container.adapter = DockOutAdapter(this, data.ongoing_trip_data.container_ids)
        rv_container.setItemViewCacheSize(data.ongoing_trip_data.container_ids.size)
        cancel.setOnClickListener {
            dialog.dismiss()
        }
        dock_out.setOnClickListener {
            var adapter = rv_container.adapter as DockOutAdapter
            var request = DockOutRequest()
            request.container_ids = adapter.getListData()
            if (shouldProceed(adapter.getListData()))
                DockOut(dialog, data.ongoing_trip_data._id, data._id, request)
            else
                CommonUtils.showToast(applicationContext, "Fill all Container Ids")
        }
        dialog.setCancelable(true)
        dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
    }

    fun showNewTripDialog() {
        var dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_add_trip)

        var radioGroup: RadioGroup = dialog.findViewById(R.id.rg_new_trip)
        var rb_dock: RadioButton = dialog.findViewById(R.id.rb_dock)
        var rb_trip: RadioButton = dialog.findViewById(R.id.rb_manual_trip)

        var cancel: Button = dialog.findViewById(R.id.btn_cancel)
        var btn_continue: Button = dialog.findViewById(R.id.btn_continue)

        btn_continue.setOnClickListener {
            if (rb_dock.isChecked) {
                dialog.dismiss()
                showNewDockDialog()
            } else if (rb_trip.isChecked) {
                var intent = Intent(this, TripActivity::class.java)
                startActivity(intent)
            }

        }

        cancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setCancelable(true)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
    }

    fun showNewDockDialog() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_new_dock)

        val cancel: Button = dialog.findViewById(R.id.btn_cancel)
        val create: Button = dialog.findViewById(R.id.btn_create)
        val et_dock: EditText = dialog.findViewById(R.id.et_dock_id)
        cancel.setOnClickListener {
            dialog.dismiss()
        }

        create.setOnClickListener {
            if (TextUtils.isEmpty(et_dock.text)) {
                CommonUtils.showToast(applicationContext, "Empty Field")
            } else {
                AddDock(et_dock.text.toString(), dialog)
            }
        }
        dialog.setCancelable(true)
        dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
    }

    fun showUnloadingDialog(data: DocksResult){
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_unloading)

        val cancel: Button = dialog.findViewById(R.id.btn_cancel)
        val create: Button = dialog.findViewById(R.id.btn_submit)
        val et_boxes: EditText = dialog.findViewById(R.id.et_no_boxes)
        val et_other: EditText = dialog.findViewById(R.id.et_other)
        cancel.setOnClickListener {
            dialog.dismiss()
        }

        create.setOnClickListener {
            dialog.dismiss()
            UpdateLoadingEnd(data,UpdateLoadingEnd(System.currentTimeMillis(),et_boxes.text.toString(),et_other.text.toString()))
        }
        dialog.setCancelable(true)
        dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()

    }


    fun AddDock(dock: String, dialog: Dialog) {
        apiUtility.AddDock(this, true, dock, object : APIUtility.APIResponseListener<BaseResponse> {
            override fun onReceiveResponse(response: BaseResponse?) {
                CommonUtils.showToast(applicationContext, response!!.message)
                dialog.dismiss()
            }
        })
    }


    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (rv_home.adapter is DockAdapter) {
            val adapter = rv_home.adapter as DockAdapter
            adapter.filter.filter(s)
        } else if (rv_home.adapter is OpenTransactionAdapter) {
            val adapter = rv_home.adapter as OpenTransactionAdapter
            adapter.filter.filter(s)
        }
    }

    fun shouldProceed(data: ArrayList<ContainerId>): Boolean {
        if (data.size == 0) return true
        for (pos in data.indices) {
            if (data[pos].container_id == null)
                return false
        }

        return true
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        rv_home.adapter =
            OpenTransactionAdapter(this@MainActivity, if (isChecked) ar_incoming else ar_outgoing)
    }
*/

}