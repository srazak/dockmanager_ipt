package com.intutrack.myntradockmanager.UI.dock

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.databinding.FragmentDockBinding

class DockFragment : Fragment(R.layout.fragment_dock) {
    private var dockFragmentBinding: FragmentDockBinding? = null
    private val binding get() = dockFragmentBinding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dockFragmentBinding = FragmentDockBinding.bind(view)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dockFragmentBinding = null
    }
}