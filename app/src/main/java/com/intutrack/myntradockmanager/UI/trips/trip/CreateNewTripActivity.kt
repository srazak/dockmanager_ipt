package com.intutrack.myntradockmanager.UI.trips.trip

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.databinding.ActivityCreateNewTripBinding

class CreateNewTripActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityCreateNewTripBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_new_trip)
    }
}