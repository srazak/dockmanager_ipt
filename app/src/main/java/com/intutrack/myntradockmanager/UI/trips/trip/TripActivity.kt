package com.intutrack.myntradockmanager.UI.trips.trip

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.tabs.TabLayoutMediator
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.databinding.ActivityTrip2Binding

class TripActivity : AppCompatActivity() {
    private var trip2Binding: ActivityTrip2Binding? = null
    private val binding get() = trip2Binding!!
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trip2Binding = ActivityTrip2Binding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarTrip)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragment_trip_Main_Container) as NavHostFragment
        navController = navHostFragment.navController
    }

    override fun onNavigateUp(): Boolean {
        return navController.navigateUp()
    }

    override fun onDestroy() {
        super.onDestroy()
        trip2Binding = null
    }
}