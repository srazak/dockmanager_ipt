package com.intutrack.myntradockmanager.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.intutrack.myntradockmanager.Bean.Login.LoadType
import com.intutrack.myntradockmanager.R
import java.util.*


class LoadTypeSpinnerAdapter(context: Context, data: ArrayList<LoadType>) : BaseAdapter() {

    var list = ArrayList<LoadType>()
    var mContext: Context
    private var inflater: LayoutInflater? = null

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(i: Int): LoadType {
        return list[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }


    override fun getView(i: Int, convertview: View?, viewGroup: ViewGroup): View {
        var view = convertview
        if (convertview == null) {
            view = inflater!!.inflate(R.layout.spinner_layout, viewGroup, false)
        }
        val label = view!!.findViewById<TextView>(R.id.spinnerTarget)
        label.text = list[i].load_type.value
        return view
    }

    fun getSelection(pos: Int): LoadType {
        return list[pos]
    }

    init {
        list = data
        mContext = context
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}