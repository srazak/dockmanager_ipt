package com.intutrack.myntradockmanager.UI.menu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.databinding.FragmentMenuBinding

class MenuFragment : Fragment(R.layout.fragment_menu) {
    private var menuFragmentBinding: FragmentMenuBinding? = null
    private val binding get() = menuFragmentBinding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        menuFragmentBinding = FragmentMenuBinding.bind(view)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        menuFragmentBinding = null
    }
}