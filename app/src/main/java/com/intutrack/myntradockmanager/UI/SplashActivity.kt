package com.intutrack.myntradockmanager.UI

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.intutrack.myntradockmanager.MainActivity
import com.intutrack.myntradockmanager.Preference.PrefEntities
import com.intutrack.myntradockmanager.Preference.Preferences
import com.intutrack.myntradockmanager.R

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Preferences.getPreference_boolean(this@SplashActivity, PrefEntities.ISLOGIN)) {
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        } else {
            startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
            finish()
        }
    }
}