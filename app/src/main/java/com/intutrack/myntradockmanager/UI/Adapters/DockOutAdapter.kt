package com.intutrack.myntradockmanager.UI.Adapters

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.myntradockmanager.Bean.OpenTransactions.ContainerId
import com.intutrack.myntradockmanager.R


class DockOutAdapter(val context: Context, val data: ArrayList<ContainerId>) :
    RecyclerView.Adapter<DockOutAdapter.MyViewHolder>() {

    var list: ArrayList<ContainerId> = ArrayList()

    init {
        list = data
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var et_container_id: EditText


        init {
            et_container_id = itemView.findViewById(R.id.et_container)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_dock_out, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var model = list[position]

        /*if (model.container_id!=null)
            holder.et_container_id.setText(model.container_id) */

        holder.et_container_id.hint = model.lane

        holder.et_container_id.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                list[position].container_id = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    fun getListData(): ArrayList<ContainerId> {
        return list
    }
}