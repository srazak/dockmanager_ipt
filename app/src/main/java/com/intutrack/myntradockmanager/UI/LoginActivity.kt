package com.intutrack.myntradockmanager.UI

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.intutrack.intutracksct.Retrofit.Helper.APIUtility
import com.intutrack.intutracksct.Retrofit.IntuApplication
import com.intutrack.myntradockmanager.Bean.Login.LoginResponse
import com.intutrack.myntradockmanager.MainActivity
import com.intutrack.myntradockmanager.Preference.PrefEntities
import com.intutrack.myntradockmanager.Preference.Preferences
import com.intutrack.myntradockmanager.Utilities.CommonUtils
import com.intutrack.myntradockmanager.databinding.ActivityLoginBinding
import java.util.*

class LoginActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityLoginBinding.inflate(layoutInflater)
    }
    private lateinit var apiUtility: APIUtility

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        apiUtility = IntuApplication.apiUtility!!
        checkLogin()
        checkAndRequestPermissions()
        /*    apiUtility = IntuApplication.apiUtility!!
            checkLogin()
            checkAndRequestPermissions()
            init()*/
    }

    private fun goToMainActivity() {
        Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(this)
        }
    }

    private fun checkLogin() {
        if (Preferences.getPreference_boolean(applicationContext, PrefEntities.ISLOGIN)) {
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }


    private fun checkAndRequestPermissions() {
        val callPhone = ContextCompat.checkSelfPermission(
            this@LoginActivity, Manifest.permission.CALL_PHONE
        )
        val listPermissionsNeeded: MutableList<String> = ArrayList()
        if (callPhone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE)
        }
        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this@LoginActivity, listPermissionsNeeded.toTypedArray(), 1233
            )
            //return false;
        }

        //return true;
    }

    private fun validation() {
        binding.etUser.error = null
        binding.etPass.error = null

        val email = binding.etUser.text.toString().trim { it <= ' ' }

        var cancel = false
        var focusView: View? = null
        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            if (email.contains("@")) {
                if (!CommonUtils.isEmailValid(email)) {
                    binding.etUser.error = "Invalid Email Address"
                    focusView = binding.etUser
                    cancel = true
                }
            } else {
                if (!CommonUtils.isValidMobNumber(email)) {
                    binding.etUser.error = "Invalid Login"
                    focusView = binding.etUser
                    cancel = true
                }
            }

        } else if (TextUtils.isEmpty(binding.etPass.text.toString())) {
            binding.etPass.error = "Enter a Password"
            focusView = binding.etPass
            cancel = true
        }

        if (cancel) {
            focusView!!.requestFocus()

        } else {
            login(email, binding.etPass.text.toString())
        }
    }


    private fun login(email: String, password: String) {
        apiUtility.Login(this, true, email, password, object : APIUtility.APIResponseListener<LoginResponse> {
            override fun onReceiveResponse(response: LoginResponse?) {
                Preferences.setPreference(applicationContext, PrefEntities.ISLOGIN, true)
                Preferences.setPreference(
                    applicationContext, PrefEntities.WAREHOUSE, response!!.result[0].warehouse_config[0].name
                )
                Preferences.setPreference(
                    applicationContext, PrefEntities.EMAIL, response.result[0].username
                )

                Preferences.storeWarehouse(
                    applicationContext, response.result[0].warehouse_config[0].dest_warehouse, PrefEntities.DESTINATIONS
                )
                Preferences.storeKeyValue(
                    applicationContext, response.result[0].warehouse_config[0].placement_type, PrefEntities.PLACEMENT_TYPE
                )
                Preferences.storeKeyValue(
                    applicationContext, response.result[0].warehouse_config[0].vehicle_status, PrefEntities.VEHICLE_STATUS
                )
                Preferences.storeKeyValue(
                    applicationContext, response.result[0].warehouse_config[0].vendors, PrefEntities.VENDORS
                )
                Preferences.storeKeyValue(
                    applicationContext, response.result[0].warehouse_config[0].purpose, PrefEntities.PURPOSE
                )
                Preferences.storeKeyValue(
                    applicationContext, response.result[0].warehouse_config[0].truck_type, PrefEntities.TRUCK_TYPE
                )
                Preferences.storeClientType(
                    applicationContext, response.result[0].warehouse_config[0].client_trip_type, PrefEntities.CLIENT_TYPE
                )

                goToMainActivity()
            }
        })
    }

    fun validateAndLogin(view: View) {
        validation()
    }
}