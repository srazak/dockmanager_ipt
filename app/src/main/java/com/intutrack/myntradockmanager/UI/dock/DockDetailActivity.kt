package com.intutrack.myntradockmanager.UI.dock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.intutrack.myntradockmanager.R

class DockDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dock_detail)
    }
}