package com.intutrack.myntradockmanager.UI

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.intutracksct.Retrofit.Helper.APIUtility
import com.intutrack.myntradockmanager.Bean.BaseResponse
import com.intutrack.myntradockmanager.Bean.Docks.DockResponse
import com.intutrack.myntradockmanager.Bean.Docks.DocksResult
import com.intutrack.myntradockmanager.Bean.OpenTransactions.OpenTransactionResult
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.UI.Adapters.AssignDockAdapter
import com.intutrack.myntradockmanager.Utilities.CommonUtils

class AssignDockActivity : BaseActivity(), AssignDockAdapter.AssignDockAdapterCallbacks,
    View.OnClickListener, TextWatcher {
    lateinit var rv_docks: RecyclerView
    lateinit var et_search: EditText
    lateinit var tv_veh_no: TextView
    lateinit var btn_confirm: Button

    lateinit var model: DocksResult
    lateinit var tripData: OpenTransactionResult

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assign_dock)
        init()
    }

    internal fun init() {
        rv_docks = findViewById(R.id.rv_assign_dock)
        rv_docks.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        et_search = findViewById(R.id.et_search_dock)
        tv_veh_no = findViewById(R.id.tv_veh_no)

        btn_confirm = findViewById(R.id.btn_cofirm)
        btn_confirm.setOnClickListener(this)

        et_search.addTextChangedListener(this)

        tripData = intent.getSerializableExtra("data") as OpenTransactionResult

        GetDocks()
    }


    fun GetDocks() {
        apiUtility.GetDocks(this, true, object : APIUtility.APIResponseListener<DockResponse> {
            override fun onReceiveResponse(response: DockResponse?) {
                rv_docks.adapter = AssignDockAdapter(this@AssignDockActivity, response!!.result)
            }
        })
    }

    override fun OnSelected(data: DocksResult) {
        model = data
        btn_confirm.visibility = View.VISIBLE
    }

    override fun onClick(v: View?) {
        AssignDock()
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        val adapter = rv_docks.adapter as AssignDockAdapter
        adapter.filter.filter(s)
    }

    fun AssignDock() {
        apiUtility.AssignDock(
            this,
            true,
            model._id,
            tripData._id,
            model.place,
            object : APIUtility.APIResponseListener<BaseResponse> {
                override fun onReceiveResponse(response: BaseResponse?) {
                    CommonUtils.showToast(applicationContext, response!!.message)
                    finish()
                }
            })
    }

}