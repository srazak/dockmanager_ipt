package com.intutrack.myntradockmanager.UI.trips.trip

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.intutrack.myntradockmanager.R

class AssignNewDockActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assign_new_dock)
    }
}