package com.intutrack.myntradockmanager.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.myntradockmanager.Bean.Docks.DocksResult
import com.intutrack.myntradockmanager.R

class AssignDockAdapter(val context: Context, val data: ArrayList<DocksResult>) : RecyclerView.Adapter<AssignDockAdapter.MyViewHolder>(), Filterable {

    var list: ArrayList<DocksResult> = ArrayList()
    var mFilterList: ArrayList<DocksResult> = ArrayList()
    lateinit var callback: AssignDockAdapterCallbacks
    private var valueFilter: ValueFilter? = null

    init {
        list = data
        mFilterList = data
        callback = context as AssignDockAdapterCallbacks
    }

    override fun getFilter(): Filter {
        if (valueFilter == null) {
            valueFilter = ValueFilter()
        }
        return valueFilter as ValueFilter
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_simple_one: TextView
        var tv_simple_two: TextView
        var rl_simple: RelativeLayout

        init {
            tv_simple_one = itemView.findViewById(R.id.tv_simple_one)
            tv_simple_two = itemView.findViewById(R.id.tv_simple_two)
            rl_simple = itemView.findViewById(R.id.rl_simple)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_assign_dock, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        holder.tv_simple_one.text = model.place.toUpperCase()
        holder.tv_simple_two.text = model.tripList.size.toString()

        if (model.isSelected) {
            holder.rl_simple.background = ContextCompat.getDrawable(context, R.drawable.round_blue)
            holder.tv_simple_one.setTextColor(
                ContextCompat.getColor(
                    context, R.color.textColorWhite
                )
            )
            holder.tv_simple_two.setTextColor(
                ContextCompat.getColor(
                    context, R.color.textColorWhite
                )
            )

        } else {
            holder.rl_simple.background = ContextCompat.getDrawable(context, R.drawable.round_gray_notification)
            holder.tv_simple_one.setTextColor(ContextCompat.getColor(context, R.color.text_primary))
            holder.tv_simple_two.setTextColor(ContextCompat.getColor(context, R.color.text_primary))
        }

        holder.rl_simple.setOnClickListener {
            setSelection(position)
            callback.OnSelected(model)
        }
    }

    private inner class ValueFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()
            var filterHash: DocksResult

            if (constraint != null && constraint.length > 0) {
                val filterList = java.util.ArrayList<DocksResult>()

                for (i in mFilterList.indices) {
                    if (mFilterList[i].place.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filterHash = mFilterList[i]
                        filterList.add(filterHash)
                    }
                }
                results.count = filterList.size
                results.values = filterList
            } else {
                results.count = mFilterList.size
                results.values = mFilterList
            }
            return results
        }

        override fun publishResults(charSequence: CharSequence, results: Filter.FilterResults) {
            list = results.values as java.util.ArrayList<DocksResult>
            notifyDataSetChanged()
        }
    }


    private fun setSelection(pos: Int) {

        for (item in list.indices) {
            list[item].isSelected = item == pos
        }

        notifyDataSetChanged()
    }

    interface AssignDockAdapterCallbacks {
        fun OnSelected(data: DocksResult)
    }


}