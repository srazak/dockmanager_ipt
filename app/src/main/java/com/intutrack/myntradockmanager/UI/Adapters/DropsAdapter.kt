package com.intutrack.myntradockmanager.UI.Adapters

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.myntradockmanager.R

class DropsAdapter(val context: Context) : RecyclerView.Adapter<DropsAdapter.MyViewHolder>() {

    var list = ArrayList<String>()

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_time: TextView
        var tv_veh: TextView
        var rl_root: RelativeLayout

        init {
            tv_time = itemView.findViewById(R.id.tv_time)
            tv_veh = itemView.findViewById(R.id.tv_veh)
            rl_root = itemView.findViewById(R.id.rl_root)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_vehicle, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var model = list[position]

        var drop = position + 1

        holder.tv_veh.text = "Drop $drop"
        holder.tv_time.text = model
        holder.rl_root.setOnClickListener {
            removeDrop(position)
        }
    }

    fun addDrop(drop: String) {
        list.add(drop)
        notifyDataSetChanged()
    }

    fun removeDrop(pos: Int) {
        list.removeAt(pos)
        notifyDataSetChanged()
    }

    fun getQuery(): String {

        return TextUtils.join(",", list)
    }

    fun getListData(): ArrayList<String> {
        return list
    }
}