package com.intutrack.myntradockmanager.UI

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.intutrack.intutracksct.Retrofit.Helper.APIUtility
import com.intutrack.myntradockmanager.Bean.AddTrip.Driver
import com.intutrack.myntradockmanager.Bean.AddTrip.TripData
import com.intutrack.myntradockmanager.Bean.AddTrip.TripRequest
import com.intutrack.myntradockmanager.Bean.BaseResponse
import com.intutrack.myntradockmanager.Bean.DropDown.DropDownResponse
import com.intutrack.myntradockmanager.Bean.Login.ValueMode
import com.intutrack.myntradockmanager.Bean.Login.ValueWarehouse
import com.intutrack.myntradockmanager.Preference.PrefEntities
import com.intutrack.myntradockmanager.Preference.Preferences
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.UI.Adapters.*
import com.intutrack.myntradockmanager.Utilities.CommonUtils

class TripActivity : BaseActivity(), View.OnClickListener {

    private lateinit var sp_lane: Spinner
    private lateinit var et_truck: EditText
    private lateinit var et_driver_contact: EditText
    private lateinit var et_driver_name: EditText
    private lateinit var sp_veh_status: Spinner
    private lateinit var sp_vendor: Spinner
    private lateinit var sp_purpose: Spinner
    private lateinit var sp_placement_type: Spinner
    private lateinit var sp_drops: Spinner
    private lateinit var sp_vehicle_type: Spinner
    private lateinit var sp_client_type: Spinner
    private lateinit var sp_edge_type: Spinner
    private lateinit var sp_load_type: Spinner
    private lateinit var sp_mode: Spinner
    private lateinit var rv_drops: RecyclerView
    private lateinit var btn_add: Button
    private lateinit var btn_submit: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip)
        sp_lane = findViewById(R.id.sp_lane)
        et_truck = findViewById(R.id.et_truck)
        et_driver_contact = findViewById(R.id.et_driver_contact)
        et_driver_name = findViewById(R.id.et_driver_name)
        btn_add = findViewById(R.id.btn_add)
        btn_submit = findViewById(R.id.btn_submit)
        btn_add.setOnClickListener(this)
        btn_submit.setOnClickListener(this)
        sp_veh_status = findViewById(R.id.sp_veh_status)
        sp_vendor = findViewById(R.id.sp_vendor)
        sp_placement_type = findViewById(R.id.sp_placement_type)
        sp_vehicle_type = findViewById(R.id.sp_vehicle_type)
        sp_purpose = findViewById(R.id.sp_purpose)
        sp_drops = findViewById(R.id.sp_drops)
        sp_client_type = findViewById(R.id.sp_client_type)
        sp_edge_type = findViewById(R.id.sp_edge_type)
        sp_load_type = findViewById(R.id.sp_load_type)
        sp_mode = findViewById(R.id.sp_mode)
        rv_drops = findViewById(R.id.rv_drops)
        rv_drops.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_drops.isNestedScrollingEnabled = false
        rv_drops.adapter = DropsAdapter(this)

        sp_veh_status.adapter = MySpinnerAdapter(this, Preferences.loadKeyValue(applicationContext, PrefEntities.VEHICLE_STATUS))
        sp_vendor.adapter = MySpinnerAdapter(this, Preferences.loadKeyValue(applicationContext, PrefEntities.VENDORS))
        sp_placement_type.adapter = MySpinnerAdapter(this, Preferences.loadKeyValue(applicationContext, PrefEntities.PLACEMENT_TYPE))
        sp_purpose.adapter = MySpinnerAdapter(this, Preferences.loadKeyValue(applicationContext, PrefEntities.PURPOSE))
        sp_drops.adapter = DestinationSpinnerAdapter(this, Preferences.loadWarehouse(applicationContext, PrefEntities.DESTINATIONS))
        sp_lane.adapter = DestinationSpinnerAdapter(this, Preferences.loadWarehouse(applicationContext, PrefEntities.DESTINATIONS))
        sp_vehicle_type.adapter = MySpinnerAdapter(this, Preferences.loadKeyValue(applicationContext, PrefEntities.TRUCK_TYPE))
        sp_client_type.adapter = ClientTypeSpinnerAdapter(this, Preferences.loadClientType(applicationContext, PrefEntities.CLIENT_TYPE))

        sp_client_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var adapter = sp_client_type.adapter as ClientTypeSpinnerAdapter
                sp_edge_type.adapter = EdgeTypeSpinnerAdapter(this@TripActivity, adapter.getItem(position).edge_type)
            }

        }

        sp_edge_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var adapter = sp_edge_type.adapter as EdgeTypeSpinnerAdapter
                sp_load_type.adapter = LoadTypeSpinnerAdapter(this@TripActivity, adapter.getItem(position).loadType)
            }

        }

        sp_load_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var adapter = sp_load_type.adapter as LoadTypeSpinnerAdapter
                sp_mode.adapter = ModeSpinnerAdapter(this@TripActivity, adapter.getItem(position).mode)
            }

        }

        sp_mode.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {

            }

        }

    }


    fun Validate() {
        var cancel = false
        var focusView: View? = null

        if (TextUtils.isEmpty(et_truck.text)) {
            et_truck.error = getString(R.string.form_error)
            focusView = et_truck
            cancel = true
        }

        if (TextUtils.isEmpty(et_driver_contact.text)) {
            et_driver_contact.error = getString(R.string.form_error)
            focusView = et_driver_contact
            cancel = true
        }


        if (cancel) {
            focusView?.requestFocus()
        } else {

            val dropadapter = rv_drops.adapter as DropsAdapter
            var driver = Driver()
            driver.name = et_driver_name.text.toString()
            driver.phone = et_driver_contact.text.toString()
            var data = TripData(
                et_truck.text.toString(),
                driver,
                getSpinnerValue(sp_veh_status),
                getSpinnerValue(sp_vendor),
                getSpinnerValue(sp_vehicle_type),
                dropadapter.getListData(),
                getDestination(sp_lane),
                getSpinnerValue(sp_placement_type),
                getSpinnerValue(sp_purpose),
                getSpinnerValue(sp_client_type),
                getSpinnerValue(sp_edge_type),
                getSpinnerValue(sp_load_type),
                getMode(sp_mode)
            )

            var listData: ArrayList<TripData> = ArrayList()
            listData.add(data)

            var request = TripRequest()
            request.data = listData

            AddTrip(request)

        }
    }


    fun AddTrip(request: TripRequest) {
        apiUtility.Addtrip(this, true, request, object : APIUtility.APIResponseListener<BaseResponse> {
            override fun onReceiveResponse(response: BaseResponse?) {
                CommonUtils.showToast(applicationContext, "Trip Added")
                finish()
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {

            R.id.btn_add -> {
                val dropadapter = rv_drops.adapter as DropsAdapter
                val spinneradapter = sp_drops.adapter as DestinationSpinnerAdapter
                dropadapter.addDrop(spinneradapter.getSelection(sp_drops.selectedItemPosition).key)
                GetDropDown(Preferences.getPreference(this, PrefEntities.WAREHOUSE) + "," + dropadapter.getQuery())
            }

            R.id.btn_submit -> {
                Validate()
            }
        }
    }

    fun getSpinnerValue(spinner: Spinner): String {
        when (spinner.adapter) {
            is ClientTypeSpinnerAdapter -> {
                val adapter = spinner.adapter as ClientTypeSpinnerAdapter
                return adapter.getSelection(spinner.selectedItemPosition).client_trip_type.key
            }
            is EdgeTypeSpinnerAdapter -> {
                val adapter = spinner.adapter as EdgeTypeSpinnerAdapter
                return adapter.getSelection(spinner.selectedItemPosition).edge_type.key
            }
            is LoadTypeSpinnerAdapter -> {
                val adapter = spinner.adapter as LoadTypeSpinnerAdapter
                return adapter.getSelection(spinner.selectedItemPosition).load_type.key
            }
            else -> {
                val adapter = spinner.adapter as MySpinnerAdapter
                return adapter.getSelection(spinner.selectedItemPosition)
            }
        }

    }

    fun getMode(spinner: Spinner): ValueMode {
        val adapter = spinner.adapter as ModeSpinnerAdapter
        return adapter.getSelection(spinner.selectedItemPosition).modeData.valueMode
    }

    fun getDestination(spinner: Spinner): ValueWarehouse {
        return if (sp_lane.count > 0) {
            val adapter = spinner.adapter as DestinationSpinnerAdapter
            adapter.getSelection(spinner.selectedItemPosition).value
        } else ValueWarehouse()

    }


    fun GetDropDown(location: String) {
        CommonUtils.log("ALIEN", location)
        apiUtility.GetDropDown(this, true, location, object : APIUtility.APIResponseListener<DropDownResponse> {
            override fun onReceiveResponse(response: DropDownResponse?) {
                sp_lane.adapter = DestinationSpinnerAdapter(this@TripActivity, response!!.result)
            }
        })
    }

}