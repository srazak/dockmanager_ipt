package com.intutrack.myntradockmanager.UI.trips.trip

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.databinding.FragmentTripsBinding
import kotlinx.android.synthetic.main.fragment_trips.*


class TripsFragment : Fragment(R.layout.fragment_trips) {
    private var fragmentTripsBinding: FragmentTripsBinding? = null  //Yeh property ko destroy krne ke liye onDestroyView me
    private val binding get() = fragmentTripsBinding!!  //Yeh viewBinding ka val property  getter lgaakr chalaaya hai isko

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentTripsBinding = FragmentTripsBinding.bind(view)
        //Setup Tabs in TabLayout and add Callback on tabSelect
        setupTabs()

        //Ab FAB button se createNewTrip ka page kholna hai navController ke madad se
        binding.fabBtnCreateNewTrip.setOnClickListener {
            findNavController().navigate(R.id.action_tripsFragment_to_createNewTripActivity)
        }

        //Ab recyclerView ko setup krenge
        binding.rvTrips.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
    }

    private fun setupTabs() {
        //Isme tabs add krenge text ke saath
        binding.tripTabLayout.addTab(binding.tripTabLayout.newTab().setText("Inbound"), 0, true)
        binding.tripTabLayout.addTab(binding.tripTabLayout.newTab().setText("Outbound"))

        //Isme apan callback lga rhe hai jab kabhi tab select ho
        binding.tripTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {

                    }
                    else -> {

                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                Log.d(TAG, "onTabUnselected: ")
                return
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                Log.d(TAG, "onTabReselected: ")
                return
            }

        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        //Yaha pr viewBinding ki property null kr di jaayegi isse resources kharcha nhi honge
        fragmentTripsBinding = null
    }

    companion object {
        private const val TAG = "TripsFragment"
    }
}