package com.intutrack.myntradockmanager.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.intutrack.myntradockmanager.Bean.Login.ClientTripType
import com.intutrack.myntradockmanager.R
import java.util.*

class ClientTypeSpinnerAdapter(context: Context, data: ArrayList<ClientTripType>) : BaseAdapter() {

    var list = ArrayList<ClientTripType>()
    var mContext: Context
    private var inflater: LayoutInflater? = null

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(i: Int): ClientTripType {
        return list[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }


    override fun getView(i: Int, convertview: View?, viewGroup: ViewGroup): View {
        var view = convertview
        if (convertview == null) {
            view = inflater!!.inflate(R.layout.spinner_layout, viewGroup, false)
        }
        val label = view!!.findViewById<TextView>(R.id.spinnerTarget)
        label.text = list[i].client_trip_type.value
        return view
    }

    fun getSelection(pos: Int): ClientTripType {
        return list[pos]
    }

    init {
        list = data
        mContext = context
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}