package com.intutrack.myntradockmanager.UI.trips.trip

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.intutrack.myntradockmanager.R
import com.intutrack.myntradockmanager.UI.trips.adapters.NavHostFragmentAdapter
import com.intutrack.myntradockmanager.databinding.FragmentTripViewPagerBinding

class TripViewPagerFragment : Fragment(R.layout.fragment_trip_view_pager) {
    private var tripViewPagerBinding: FragmentTripViewPagerBinding? = null
    private val binding get() = tripViewPagerBinding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tripViewPagerBinding = FragmentTripViewPagerBinding.bind(view)
        val tabs = arrayOf("Timeline", "Trip Details", "CheckList", "SKU")
        val navGraphIds = listOf(R.navigation.nav_timeline, R.navigation.nav_trip_details, R.navigation.nav_docs, R.navigation.nav_sku)
        binding.viewPagerTrip.adapter = NavHostFragmentAdapter(childFragmentManager, viewLifecycleOwner.lifecycle, navGraphIds)
        TabLayoutMediator(
            binding.tripDetailTabLayout, binding.viewPagerTrip
        ) { tab, position -> tab.text = tabs[position] }.attach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        tripViewPagerBinding = null
    }
}