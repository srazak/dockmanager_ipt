package com.intutrack.myntradockmanager.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.intutrack.myntradockmanager.Bean.Login.KeyValueWarehouse
import com.intutrack.myntradockmanager.R
import java.util.*


class DestinationSpinnerAdapter(context: Context, data: ArrayList<KeyValueWarehouse>) :
    BaseAdapter() {

    var list = ArrayList<KeyValueWarehouse>()
    var mContext: Context
    private var inflater: LayoutInflater? = null

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(i: Int): String {
        return list[i].key
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }


    override fun getView(i: Int, convertview: View?, viewGroup: ViewGroup): View {
        var view = convertview
        if (convertview == null) {
            view = inflater!!.inflate(R.layout.spinner_layout, viewGroup, false)
        }
        val label = view!!.findViewById<TextView>(R.id.spinnerTarget)
        label.text = list[i].key
        return view
    }

    fun getSelection(pos: Int): KeyValueWarehouse {
        return if (pos >= 0) list[pos] else KeyValueWarehouse()
    }

    init {
        list = data
        mContext = context
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}