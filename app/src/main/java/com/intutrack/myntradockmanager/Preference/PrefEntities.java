package com.intutrack.myntradockmanager.Preference;

public class PrefEntities {
    public static final String EMAIL = "EMAIL";
    public static final String CLIENT = "CLIENT";
    public static final String WAREHOUSE = "WAREHOUSE";
    public static final String USERNAME = "USERNAME";
    public static final String SUBCLIENT = "SUBCLIENT";
    public static final String VENDORS = "VENDORS";
    public static final String DESTINATIONS = "DESTINATIONS";
    public static final String TRUCK_TYPE = "TRUCK_TYPE";
    public static final String VEHICLE_STATUS = "VEHICLE_STATUS";
    public static final String PURPOSE = "PURPOSE";
    public static final String PLACEMENT_TYPE = "PLACEMENT_TYPE";
    public static final String AUTH_TOKEN = "AUTH_TOKEN";
    public static final String ISLOGIN = "IS_LOGIN";
    public static final String TRIP_TYPE = "TRIP_TYPE";
    public static final String CLIENT_TYPE = "CLIENT_TYPE";

    //Location
    public static final String FIRST_START_COMPLETE = "FIRST_START_";
    public static final String FIRST_TRACK_COMPLETE = "FIRST_TRACK_COMPLETE_";
    public static final String IS_SERVICE_RUNNING = "IS_SERVICE_RUNNING_";
    public static final String TRIP_ID = "TRIP_ID";


}
