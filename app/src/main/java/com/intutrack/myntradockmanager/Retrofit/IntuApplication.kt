package com.intutrack.intutracksct.Retrofit

import android.app.Application
import android.content.Context
import com.intutrack.intutracksct.Retrofit.Helper.APIUtility
import com.intutrack.myntradockmanager.R
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump


class IntuApplication : Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        context = base
    }

    override fun onCreate() {
        super.onCreate()

        apiUtility = APIUtility(applicationContext)

        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/poppins_regular.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )

    }

    companion object {
        var apiUtility: APIUtility? = null
            private set
        private var context: Context? = null

    }


}
