package com.intutrack.intutracksct.Retrofit


import com.intutrack.myntradockmanager.Bean.AddTrip.TripRequest
import com.intutrack.myntradockmanager.Bean.BaseResponse
import com.intutrack.myntradockmanager.Bean.Docks.CreateDockRequest
import com.intutrack.myntradockmanager.Bean.Docks.DockOutRequest
import com.intutrack.myntradockmanager.Bean.Docks.DockResponse
import com.intutrack.myntradockmanager.Bean.DropDown.DropDownResponse
import com.intutrack.myntradockmanager.Bean.Login.LoginResponse
import com.intutrack.myntradockmanager.Bean.OpenTransactions.OpenTransactionsResponse
import com.intutrack.myntradockmanager.Bean.UpdateTrip.UpdateLoadingEnd
import com.intutrack.myntradockmanager.Bean.UpdateTrip.UpdateLoadingStart
import retrofit2.Call
import retrofit2.http.*

interface APIService {

    @GET("login")
    fun Login(): Call<LoginResponse>

    @POST("v2/trips/submit/new")
    fun AddTrip(
        @Body tripRequest: TripRequest,
        @Query("warehouse") warehouse: String
    ): Call<BaseResponse>

    @PUT("v2/trips/beta")
    fun UpdateLoadingStart(
        @Body updateLoadingStart: UpdateLoadingStart,
        @Query("tripId") tripId: String
    ): Call<BaseResponse>

    @PUT("v2/trips/beta")
    fun UpdateLoadingEnd(
        @Body updateLoadingEnd: UpdateLoadingEnd,
        @Query("tripId") tripId: String
    ): Call<BaseResponse>

    @GET("v2/docks/unassigned/trips")
    fun GetOpenTransactions(
        @Query("warehouse") warehouse: String
    ): Call<OpenTransactionsResponse>

    @GET("v2/docks")
    fun GetDocks(
        @Query("warehouse") warehouse: String
    ): Call<DockResponse>

    @POST("v2/docks/{dock_id}/dockout")
    fun DockOut(
        @Path("dock_id") dock_id: String,
        @Query("tripId") tripId: String,
        @Body tripRequest: DockOutRequest
    ): Call<BaseResponse>

    @POST("v2/docks/{dock_id}/dockin")
    fun DockIn(
        @Path("dock_id") dock_id: String,
        @Query("tripId") tripId: String,
        @Body tripRequest: TripRequest
    ): Call<BaseResponse>

    @PATCH("v2/docks/{tripId}/assign/{dockid}")
    fun AssignDock(
        @Path("dockid") dock_id: String,
        @Path("tripId") trip_id: String,
        @Query("dock") dock: String,
        @Query("warehouse") warehouse: String
    ): Call<BaseResponse>

    @POST("v2/docks/new")
    fun CreateDock(
        @Body request: CreateDockRequest,
        @Query("warehouse") warehouse: String
    ): Call<BaseResponse>

    @GET("v2/trips/dropdown/location")
    fun GetDropDown(
        @Query("locations") locations: String
    ): Call<DropDownResponse>
}
