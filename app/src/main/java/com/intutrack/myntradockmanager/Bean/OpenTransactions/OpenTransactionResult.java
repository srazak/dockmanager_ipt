package com.intutrack.myntradockmanager.Bean.OpenTransactions;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.Bean.AddTrip.Driver;

import java.io.Serializable;
import java.util.ArrayList;

public class OpenTransactionResult implements Serializable {

    @SerializedName("_id")
    String _id;

    @SerializedName("client")
    String client;

    @SerializedName("warehouse")
    String warehouse;

    @SerializedName("running")
    boolean running;

    @SerializedName("submitted")
    boolean submitted;

    @SerializedName("accepted")
    boolean accepted;

    @SerializedName("vehicle")
    String vehicle;

    @SerializedName("trip_type")
    String trip_type;

    @SerializedName("assigned_dock")
    String assigned_dock;

    @SerializedName("driver")
    Driver driver;

    @SerializedName("verifiedDocs")
    String verifiedDocs = "";

    @SerializedName("loadingUnloadingEnd")
    long loadingUnloadingStartEnd = 0;

    @SerializedName("loadingUnloadingStart")
    long loadingUnloadingStart = 0;

    @SerializedName("no_of_boxes")
    String no_of_boxes;

    @SerializedName("other")
    String other;

    @SerializedName("vehicle_status")
    String vehicle_status;

    @SerializedName("latestStatus")
    String latestStatus;

    @SerializedName("gate_in_time_epoch")
    long gate_in_time_epoch = 0;

    @SerializedName("start_time_epoch")
    long start_time_epoch = 0;

    @SerializedName("dock_time_epoch")
    long dock_time_epoch;

    @SerializedName("container_ids")
    ArrayList<ContainerId> container_ids = new ArrayList<>();
    boolean selected = false;

    public String getVerifiedDocs() {
        return verifiedDocs;
    }

    public void setVerifiedDocs(String verifiedDocs) {
        this.verifiedDocs = verifiedDocs;
    }

    public long getLoadingUnloadingStartEnd() {
        return loadingUnloadingStartEnd;
    }

    public void setLoadingUnloadingStartEnd(long loadingUnloadingStartEnd) {
        this.loadingUnloadingStartEnd = loadingUnloadingStartEnd;
    }

    public long getLoadingUnloadingStart() {
        return loadingUnloadingStart;
    }

    public void setLoadingUnloadingStart(long loadingUnloadingStart) {
        this.loadingUnloadingStart = loadingUnloadingStart;
    }

    public String getNo_of_boxes() {
        return no_of_boxes;
    }

    public void setNo_of_boxes(String no_of_boxes) {
        this.no_of_boxes = no_of_boxes;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public ArrayList<ContainerId> getContainer_ids() {
        return container_ids;
    }

    public void setContainer_ids(ArrayList<ContainerId> container_ids) {
        this.container_ids = container_ids;
    }

    public long getDock_time_epoch() {
        return dock_time_epoch;
    }

    public void setDock_time_epoch(long dock_time_epoch) {
        this.dock_time_epoch = dock_time_epoch;
    }

    public long getGate_in_time_epoch() {
        return gate_in_time_epoch;
    }

    public void setGate_in_time_epoch(long gate_in_time_epoch) {
        this.gate_in_time_epoch = gate_in_time_epoch;
    }

    public long getStart_time_epoch() {
        return start_time_epoch;
    }

    public void setStart_time_epoch(long start_time_epoch) {
        this.start_time_epoch = start_time_epoch;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getLatestStatus() {
        return latestStatus;
    }

    public void setLatestStatus(String latestStatus) {
        this.latestStatus = latestStatus;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public boolean isSubmitted() {
        return submitted;
    }

    public void setSubmitted(boolean submitted) {
        this.submitted = submitted;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getTrip_type() {
        return trip_type;
    }

    public void setTrip_type(String trip_type) {
        this.trip_type = trip_type;
    }

    public String getAssigned_dock() {
        return assigned_dock;
    }

    public void setAssigned_dock(String assigned_dock) {
        this.assigned_dock = assigned_dock;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getVehicle_status() {
        return vehicle_status;
    }

    public void setVehicle_status(String vehicle_status) {
        this.vehicle_status = vehicle_status;
    }
}
