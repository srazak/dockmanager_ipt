package com.intutrack.myntradockmanager.Bean.Login;

import com.google.gson.annotations.SerializedName;

public class KeyValue {

    @SerializedName("key")
    String key;

    @SerializedName("value")
    String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
