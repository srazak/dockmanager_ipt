package com.intutrack.myntradockmanager.Bean.Login;

import com.google.gson.annotations.SerializedName;

public class Mode {

    @SerializedName("mode")
    ModeData modeData;

    public ModeData getModeData() {
        return modeData;
    }

    public void setModeData(ModeData modeData) {
        this.modeData = modeData;
    }
}
