package com.intutrack.myntradockmanager.Bean.Docks;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.Bean.OpenTransactions.OpenTransactionResult;

import java.io.Serializable;
import java.util.ArrayList;

public class DocksResult implements Serializable {

    @SerializedName("_id")
    String _id;

    @SerializedName("type")
    String type;

    @SerializedName("place")
    String place;

    @SerializedName("trip_list")
    ArrayList<TripList> tripList = new ArrayList<>();

    @SerializedName("ongoing_trip_data")
    OpenTransactionResult ongoing_trip_data;

    @SerializedName("occupied")
    boolean occupied;

    boolean expanded = false;

    boolean selected = false;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public ArrayList<TripList> getTripList() {
        return tripList;
    }

    public void setTripList(ArrayList<TripList> tripList) {
        this.tripList = tripList;
    }

    public OpenTransactionResult getOngoing_trip_data() {
        return ongoing_trip_data;
    }

    public void setOngoing_trip_data(OpenTransactionResult ongoing_trip_data) {
        this.ongoing_trip_data = ongoing_trip_data;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }
}
