package com.intutrack.myntradockmanager.Bean.Login;

import com.google.gson.annotations.SerializedName;

public class ValueMode {

    @SerializedName("mode")
    String mode;

    @SerializedName("sct_enabled")
    boolean sct_boolean = false;

    @SerializedName("ipt_enabled")
    boolean ipt_enabled = false;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public boolean isSct_boolean() {
        return sct_boolean;
    }

    public void setSct_boolean(boolean sct_boolean) {
        this.sct_boolean = sct_boolean;
    }

    public boolean isIpt_enabled() {
        return ipt_enabled;
    }

    public void setIpt_enabled(boolean ipt_enabled) {
        this.ipt_enabled = ipt_enabled;
    }
}
