package com.intutrack.myntradockmanager.Bean.Login;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WarehouseConfig {

    @SerializedName("name")
    String name;

    @SerializedName("vendors")
    ArrayList<KeyValue> vendors = new ArrayList<>();

    @SerializedName("dest_warehouse")
    ArrayList<KeyValueWarehouse> dest_warehouse = new ArrayList<>();

    @SerializedName("truck_type")
    ArrayList<KeyValue> truck_type = new ArrayList<>();

    @SerializedName("vehicle_status")
    ArrayList<KeyValue> vehicle_status = new ArrayList<>();

    @SerializedName("purpose")
    ArrayList<KeyValue> purpose = new ArrayList<>();

    @SerializedName("placement_type")
    ArrayList<KeyValue> placement_type = new ArrayList<>();

    @SerializedName("client_trip_type")
    ArrayList<ClientTripType> client_trip_type = new ArrayList<>();

    public ArrayList<ClientTripType> getClient_trip_type() {
        return client_trip_type;
    }

    public void setClient_trip_type(ArrayList<ClientTripType> client_trip_type) {
        this.client_trip_type = client_trip_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<KeyValue> getTruck_type() {
        return truck_type;
    }

    public void setTruck_type(ArrayList<KeyValue> truck_type) {
        this.truck_type = truck_type;
    }

    public ArrayList<KeyValue> getVehicle_status() {
        return vehicle_status;
    }

    public void setVehicle_status(ArrayList<KeyValue> vehicle_status) {
        this.vehicle_status = vehicle_status;
    }

    public ArrayList<KeyValue> getPurpose() {
        return purpose;
    }

    public void setPurpose(ArrayList<KeyValue> purpose) {
        this.purpose = purpose;
    }

    public ArrayList<KeyValue> getPlacement_type() {
        return placement_type;
    }

    public void setPlacement_type(ArrayList<KeyValue> placement_type) {
        this.placement_type = placement_type;
    }

    public ArrayList<KeyValue> getVendors() {
        return vendors;
    }

    public void setVendors(ArrayList<KeyValue> vendors) {
        this.vendors = vendors;
    }

    public ArrayList<KeyValueWarehouse> getDest_warehouse() {
        return dest_warehouse;
    }

    public void setDest_warehouse(ArrayList<KeyValueWarehouse> dest_warehouse) {
        this.dest_warehouse = dest_warehouse;
    }
}
