package com.intutrack.myntradockmanager.Bean.Docks;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.Bean.OpenTransactions.OpenTransactionResult;

import java.io.Serializable;

public class TripList implements Serializable {

    @SerializedName("tripId")
    String tripId;

    @SerializedName("in_dock")
    boolean in_dock;

    @SerializedName("trip_data")
    OpenTransactionResult trip_data;

    @SerializedName("assigned_at_epoch")
    long assigned_at_epoch = 0;

    @SerializedName("dock_time_epoch")
    long dock_time_epoch = 0;

    boolean selected = false;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public long getAssigned_at_epoch() {
        return assigned_at_epoch;
    }

    public void setAssigned_at_epoch(long assigned_at_epoch) {
        this.assigned_at_epoch = assigned_at_epoch;
    }

    public long getDock_time_epoch() {
        return dock_time_epoch;
    }

    public void setDock_time_epoch(long dock_time_epoch) {
        this.dock_time_epoch = dock_time_epoch;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public boolean isIn_dock() {
        return in_dock;
    }

    public void setIn_dock(boolean in_dock) {
        this.in_dock = in_dock;
    }

    public OpenTransactionResult getTrip_data() {
        return trip_data;
    }

    public void setTrip_data(OpenTransactionResult trip_data) {
        this.trip_data = trip_data;
    }
}
