package com.intutrack.myntradockmanager.Bean.AddTrip;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TripRequest {

    @SerializedName("data")
    ArrayList<TripData> data = new ArrayList<>();

    public ArrayList<TripData> getData() {
        return data;
    }

    public void setData(ArrayList<TripData> data) {
        this.data = data;
    }

}
