package com.intutrack.myntradockmanager.Bean.AddTrip;

import com.google.gson.annotations.SerializedName;

public class StopTripRequest {

    @SerializedName("client")
    String client;

    public StopTripRequest(String client) {
        this.client = client;
    }
}
