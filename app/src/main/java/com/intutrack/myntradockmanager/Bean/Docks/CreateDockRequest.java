package com.intutrack.myntradockmanager.Bean.Docks;

import com.google.gson.annotations.SerializedName;

public class CreateDockRequest {

    @SerializedName("name")
    String name;

    public CreateDockRequest(String name) {
        this.name = name;
    }
}
