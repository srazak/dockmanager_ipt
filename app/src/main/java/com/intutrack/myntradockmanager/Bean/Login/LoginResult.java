package com.intutrack.myntradockmanager.Bean.Login;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LoginResult {

    @SerializedName("username")
    String username;

    @SerializedName("token")
    String token;

    @SerializedName("warehouse_config")
    ArrayList<WarehouseConfig> warehouse_config = new ArrayList<>();

    public ArrayList<WarehouseConfig> getWarehouse_config() {
        return warehouse_config;
    }

    public void setWarehouse_config(ArrayList<WarehouseConfig> warehouse_config) {
        this.warehouse_config = warehouse_config;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
