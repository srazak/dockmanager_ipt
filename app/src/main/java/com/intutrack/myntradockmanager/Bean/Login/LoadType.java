package com.intutrack.myntradockmanager.Bean.Login;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LoadType {

    @SerializedName("load_type")
    KeyValue load_type;

    @SerializedName("mode")
    ArrayList<Mode> mode = new ArrayList<>();

    public KeyValue getLoad_type() {
        return load_type;
    }

    public void setLoad_type(KeyValue load_type) {
        this.load_type = load_type;
    }

    public ArrayList<Mode> getMode() {
        return mode;
    }

    public void setMode(ArrayList<Mode> mode) {
        this.mode = mode;
    }
}
