package com.intutrack.myntradockmanager.Bean.AddTrip;

import com.google.gson.annotations.SerializedName;

public class Drops {

    @SerializedName("name")
    String name;

    public Drops(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
