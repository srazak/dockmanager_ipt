package com.intutrack.myntradockmanager.Bean.Login;

import com.google.gson.annotations.SerializedName;

public class KeyValueWarehouse {

    @SerializedName("key")
    String key;

    @SerializedName("value")
    ValueWarehouse value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ValueWarehouse getValue() {
        return value;
    }

    public void setValue(ValueWarehouse value) {
        this.value = value;
    }
}
