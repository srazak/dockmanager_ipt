package com.intutrack.myntradockmanager.Bean.Login;

import com.google.gson.annotations.SerializedName;
import com.intutrack.myntradockmanager.Bean.BaseResponse;

import java.util.ArrayList;

public class LoginResponse extends BaseResponse {

    @SerializedName("result")
    ArrayList<LoginResult> result = new ArrayList<>();

    public ArrayList<LoginResult> getResult() {
        return result;
    }

    public void setResult(ArrayList<LoginResult> result) {
        this.result = result;
    }
}
