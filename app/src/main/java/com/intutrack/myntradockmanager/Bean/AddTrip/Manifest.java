package com.intutrack.myntradockmanager.Bean.AddTrip;

import com.google.gson.annotations.SerializedName;

public class Manifest {

    @SerializedName("number")
    String number;

    public Manifest(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
